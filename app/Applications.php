<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applications extends Model
{
    protected $table = "Applications";
    protected $fillable = ['id','name','email','dateofbirth','gender','country','city','passportid','passportpicture','message'];
}
