<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookNow extends Model
{
    protected $table = "booknow";
    protected $fillable = ['id','firstname','lastname','user_id','customer_phone','customer_email','pick_up_date','pick_up_time',
        'estimated_duration','vehicle_type','message','booking_status'];
}
