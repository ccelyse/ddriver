<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
    protected $table = "BrandModel";
    protected $fillable = ['id','brandname_id','brandmodel'];
}
