<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerSignUp extends Model
{
    protected $table = "customers";
    protected $fillable = ['id','firstname','lastname','phone','email','password'];
}
