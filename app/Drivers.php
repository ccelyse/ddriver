<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drivers extends Model
{
    protected $table = "drivers";
    protected $fillable = ['id','driver_firstname','driver_lastname','driver_languages','driver_image','driver_status'];
}
