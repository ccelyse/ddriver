<?php

namespace App\Http\Controllers;

use App\ApplyVisaFree;
use App\Attractions;
use App\BookNow;
use App\BrandModel;
use App\CustomerSignUp;
use App\Drivers;
use App\CarFeatures;
use App\CarRenting;
use App\CarSelling;
use App\CarSellingFeatures;
use App\CarSellingPictures;
use App\Countries;
use App\district;
use App\Hireaguide;
use App\JoinMember;
use App\Province;
use App\Sector;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BackendController extends Controller
{
    public function SignIn_(){

    }
    public function AccountList(){
        $listaccount = User::all();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function Dashboard(){
        $brands = Drivers::select(DB::raw('count(id) as brands'))->value('brands');
        $countdriver = Drivers::where('driver_status', 'like', '%' ."Active" . '%')->count();
        return view('backend.Dashboard')->with(['countdriver'=>$countdriver,'brands'=>$brands]);
    }
    public function CustomersAccounts(){
        $listdrivers = CustomerSignUp::orderBy('created_at','desc')->get();
        return view('backend.Customers')->with(['listdrivers'=>$listdrivers]);
    }
    public function AddDrivers(){
        $show = Drivers::all();
        return view('backend.AddDrivers')->with(['show'=>$show]);
    }
    public function AddaDriver(Request $request){
        $all = $request->all();
        $image = $request->file('fileToUpload');
        $drivers = new Drivers();

        $drivers->driver_firstname = $request['driver_firstname'];
        $drivers->driver_lastname = $request['driver_lastname'];
        $drivers->driver_languages = $request['driver_languages'];
        $drivers->driver_status = $request['driver_status'];

        $drivers->driver_image = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $imagename);
        $drivers->save();
        return back()->with('success','you successfully uploaded a new driver');
    }
    public function UpdateDriver(Request $request){
        $id = $request['id'];
        $image = $request->file('fileToUpload');
        $updatedriver = Drivers::find($id);
        if ($request->hasFile('fileToUpload')) {
            $updatedriver->driver_firstname = $request['driver_firstname'];
            $updatedriver->driver_lastname = $request['driver_lastname'];
            $updatedriver->driver_languages = $request['driver_languages'];
            $updatedriver->driver_status = $request['driver_status'];
            $updatedriver->driver_image = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $imagename);
            $updatedriver->save();
            return back()->with('success','you successfully updated a driver');
        }else{
            $updatedriver->driver_firstname = $request['driver_firstname'];
            $updatedriver->driver_lastname = $request['driver_lastname'];
            $updatedriver->driver_languages = $request['driver_languages'];
            $updatedriver->save();
            return back()->with('success','you successfully updated a driver');
        }

    }
    public function CustomerSignUp(Request $request){
        $this->validate($request, ['firstname' => 'required', 'email' => 'required', 'phone' => 'required',
           'lastname' => 'required','password' => 'required']);

        $checkaccount = CustomerSignUp::where('email',$request['email'])->first();
        if(0 == count($checkaccount)){
            $addcustomer = new CustomerSignUp();
            $addcustomer->firstname = $request['firstname'];
            $addcustomer->lastname = $request['lastname'];
            $addcustomer->phone = $request['phone'];
            $addcustomer->email = $request['email'];
            $addcustomer->password = bcrypt($request['password']);
            $addcustomer->save();
            $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
            if($addcustomer){
                $arr = array('response_message' => 'Successfully created an account', 'response_status' => true);
            }
            return Response()->json($arr);
        }else{
            $arr = array('response_message' => 'Email is already registered', 'response_message' => false);
            return Response()->json($arr);
        }

    }
    public function Customers(){
        $listcustomers = Customer::all();
        return view ('backend.Customers')->with(['listcustomers'=>$listcustomers]);

    }
    public function CustomerSignIn(Request $request){
        $userdata = array(
            'email' => $request['email'] ,
            'password' => $request['password']
        );
        $checkaccount = CustomerSignUp::where('email',$request['email'])->value('password');
        if(Hash::check($request['password'],$checkaccount)) {
            $userdatas = CustomerSignUp::where('email',$request['email'])->get();
            foreach ($userdatas as $userdatass){
                $userinfo = [
                    'id'=>$userdatass->id,
                    'firstname'=>$userdatass->firstname,
                    'lastname'=>$userdatass->lastname,
                    'phone'=>$userdatass->phone,
                    'email'=>$userdatass->email,
                ];
            }
            return response()->json([
                'status_code'=>'200',
                'response_message'=>'success',
                'user'=>$userinfo
            ]);
        } else {
            return response()->json(['status_code'=>'201','message'=>'Ooops, account not found']);
        }
    }
    public function CustomerDetails(Request $request){
        $userdatas = CustomerSignUp::where('email',$request['email'])->get();
        foreach ($userdatas as $userdatass){
            $userinfo = [
                'id'=>$userdatass->id,
                'firstname'=>$userdatass->firstname,
                'lastname'=>$userdatass->lastname,
                'phone'=>$userdatass->phone,
                'email'=>$userdatass->email,
            ];
        }
        return response()->json([
            'status_code'=>'200',
            'response_message'=>'success',
            'user'=>$userinfo
        ]);
    }
    public function CustomerUpdate(Request $request){
        $id = $request['id'];
        $update = CustomerSignUp::where('id',$id) ->update([
            'firstname'=> $request['firstname'],
            'lastname'=> $request['lastname'],
            'phone'=> $request['phone'],
            'email'=> $request['email'],
        ]);
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($update){
            $arr = array('response_message' => 'You have successfully updated account information', 'response_status' => true);
        }

        return Response()->json($arr);
    }
    public function BookNow(Request $request){
        $this->validate($request, ['firstname' => 'required', 'customer_email' => 'required', 'customer_phone' => 'required',
            'lastname' => 'required','user_id' => 'required','pick_up_date' => 'required','pick_up_time' => 'required',
            'estimated_duration' => 'required','vehicle_type' => 'required','message' => 'required']);
        $add = new BookNow();
        $add->firstname = $request['firstname'];
        $add->lastname = $request['lastname'];
        $add->user_id = $request['user_id'];
        $add->customer_phone = $request['customer_phone'];
        $add->customer_email = $request['customer_email'];
        $add->pick_up_date = $request['pick_up_date'];
        $add->pick_up_time = $request['pick_up_time'];
        $add->estimated_duration = $request['estimated_duration'];
        $add->vehicle_type = $request['vehicle_type'];
        $add->message = $request['message'];
        $add->booking_status = "Active";
        $add->save();

        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($add){
            $arr = array('response_message' => 'You have successfully Booked a Ddriver', 'response_status' => true);
        }

        return Response()->json($arr);

    }
    public function AppBookings(){
        $show = BookNow::all();
        return view('backend.AppBookings')->with(['show'=>$show]);
    }
    public function UserBookings(Request $request){
        $email = $request['email'];
        $show  = BookNow::where('customer_email',$email)->orderBy('created_at','desc')->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return response()->json([
                'response_message' => "success",
                'response_status' =>200,
                'bookings'=>$show
            ]);

        }
    }
    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function CreateAccount_(Request $request){
        $all  = $request->all();

        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->password = bcrypt($request['password']);

        return redirect()->route('backend.AccountList')->with('Success','You have successfully Created Your Account');
    }


}
