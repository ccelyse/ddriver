<?php

namespace App\Http\Controllers;

use App\ApplyVisaFree;
use App\Countries;
use App\JoinMember;
use App\Mail\ApplicationsEmail;
use App\User;
use Illuminate\Http\Request;
use App\Mail\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller
{
    public function Home(){
        return view('auth.login');
    }
    public function Apply(){
        $listcountries = Countries::all();
        return view('Apply')->with(['listcountries'=>$listcountries]);
    }
    public function Apply_(Request $request){
        $all =$request->all();

        $request->validate([
            'fileToUpload'   => 'mimes:jpg,jpeg,png',
            'name' => 'required|string|max:140',
            'email' => 'required|string|email|max:255|unique:users',
        ]);

        $apply = new ApplyVisaFree();
        $image = $request->file('fileToUpload');
        $apply->name = $request['name'];
        $apply->email = $request['email'];
        $apply->phonenumber = $request['phonenumber'];
        $apply->dateofbirth = $request['dateofbirth'];
        $apply->gender = $request['gender'];
        $apply->country = $request['country'];
        $apply->city = $request['city'];
        $apply->passportid = $request['passportid'];
        $apply->message = $request['message'];
        $apply->passportpicture = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/passportpictures');
        $image->move($destinationPath, $imagename);

        $apply->save();

        $getOrders = $request['name'];
        $email =$request['email'];
        Mail::to($email)->send(new ApplicationsEmail($getOrders));

        return back()->with('success','Thank you applying we shall get back to you within 24 hours');

    }
    public function Login(){
        return view('backend.Login');
    }

}
