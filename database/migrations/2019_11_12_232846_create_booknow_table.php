<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooknowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booknow', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('user_id');
            $table->string('customer_phone');
            $table->string('customer_email');
            $table->string('pick_up_date');
            $table->string('pick_up_time');
            $table->string('estimated_duration');
            $table->string('vehicle_type');
            $table->string('message');
            $table->string('booking_status');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booknow');
    }
}
