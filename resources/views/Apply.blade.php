@extends('layouts.master')

@section('title', 'Visa Free Africa')

@section('content')

    @include('layouts.topmenu')
    <style>
        .content-header{
            margin-top: 40px;
            background: #000;
        }
        .content-header-title{
            color: #fff;
            border-bottom: 4px solid #0088cc;
            position: absolute;
            margin-top: 3px;
            line-height: 3;
        }
        .btn-visa {
            border-color: #000 !important;
            background-color: #000 !important;
            color: #FFF;
            margin-top: 15px;
            margin-bottom: 15px;
        }
        .card-title{
            color: #0088cc;
        }
        .btn-visablue {
            border-color: #0088cc !important;
            background-color: #0088cc !important;
            color: #FFF;
            margin-top: 15px;
            margin-bottom: 15px;
        }
        form .form-actions {
            border-top: none;
            padding: 20px 0;
            margin-top: 20px;
        }
        .help-block{
            color: red;
        }
        #success_messages{
            border-color: #28D094!important;
            background-color: #5FE0B2!important;
            color: #fff !important;
        }
        .header-navbar {
            padding: 0;
            min-height: 8rem !important;
            font-family: Quicksand,Georgia,'Times New Roman',Times,serif;
            transition: .3s ease all;
        }
        .header-navbar .navbar-container ul.nav li>a.nav-link:hover {
            color: #fff !important;
            background: #0088cc;
        }
    </style>
    <div class="content-header row">

        <div class="content-header-left col-md-4 col-12 mb-2 offset-md-1">
            <h3 class="content-header-title">55 Voices</h3>
        </div>

        <div class="content-header-right col-md-6 col-12" >
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-visa dropdown-toggle dropdown-menu-right box-shadow-2 px-2"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
            </div>
        </div>
    </div>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">

                <section id="basic-form-layouts" style="padding: 0 70px">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-form">Application Form</h4>
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form method="post" action="{{url('Apply_')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                            <label for="projectinput1">Your Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder="Name"
                                                                   name="name" value="{{ old('name') }}" required>
                                                            @if ($errors->has('name'))
                                                                <span class="help-block">
                                                                  <strong>{{ $errors->first('name') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                            <label for="projectinput2">Your Email</label>
                                                            <input type="text" id="projectinput2" class="form-control" placeholder="Email Address"
                                                                   name="email" value="{{ old('email') }}" required>
                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                                                  <strong>{{ $errors->first('email') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Phone Number</label>
                                                            <input type="number" id="projectinput2" class="form-control" placeholder="phone number"
                                                                   name="phonenumber" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" pattern="[+]{1}[0-9]{12}"  maxlength="15" class="form-control"  title="only Numbers" value="{{ old('phonenumber') }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <fieldset class="form-group">
                                                            <label for="projectinput4">Date of Birth</label>
                                                            <label>Display Month</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                                </div>
                                                                <input type="text" class="form-control dp-month-year" name="dateofbirth" value="{{ old('dateofbirth') }}" required/>
                                                            </div>
                                                        </fieldset>
                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput5">Gender</label>
                                                            <select id="projectinput5" name="gender" class="form-control"  required>
                                                                <option value="{{ old('gender') }}">{{ old('gender') }}</option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput6">Country</label>
                                                            <select id="projectinput6" name="country" class="form-control" required>
                                                                <option value="{{ old('country') }}">{{ old('country') }}</option>
                                                                @foreach($listcountries as $data)
                                                                <option value="{{$data->nicename}}">{{$data->nicename}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>


                                                </div>


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput6">City</label>
                                                            <input type="text"  id="projectinput4" class="form-control" placeholder="city" name="city" value="{{ old('city') }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput8">Passport/ID*</label>
                                                            <input type="text" id="projectinput4" class="form-control" placeholder="Passport Id" name="passportid" value="{{ old('passportid') }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-12">
                                                        <fieldset class="form-group{{ $errors->has('fileToUpload') ? ' has-error' : '' }}">
                                                            <label for="projectinput8">Passport Picture</label>
                                                            <input type="file" class="form-control-file" id="exampleInputFile" name="fileToUpload" value="{{ old('fileToUpload') }}"  required>
                                                            @if ($errors->has('fileToUpload'))
                                                                <span class="help-block">
                                                                  <strong>{{ $errors->first('fileToUpload') }}</strong>
                                                                 </span>
                                                            @endif
                                                        </fieldset>
                                                        </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput8">Your Story (1,000 words Max)*</label>
                                                            <script language="javascript" type="text/javascript">
                                                                function limitText(limitField, limitCount, limitNum) {
                                                                    if (limitField.value.length > limitNum) {
                                                                        limitField.value = limitField.value.substring(0, limitNum);
                                                                    } else {
                                                                        limitCount.value = limitNum - limitField.value.length;
                                                                    }
                                                                }
                                                            </script>

                                                            <textarea class="textareaform form-control" name="message" onKeyDown="limitText(this.form.message,this.form.countdown,1000);"
                                                                      onKeyUp="limitText(this.form.message,this.form.countdown,1000);" rows="10">{{ old('message') }}</textarea><br>
                                                            <font size="1">(Maximum characters: 1000)<br>
                                                                You have <input readonly type="text" name="countdown" size="5" value="1000"> characters left.</font>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-visablue">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    @include('layouts.footer')
@endsection
