@extends('backend.layout.master')

@section('title', 'DDriver')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->

                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Bookings</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">

                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Pick Up Date</th>
                                                <th>Pick Up Time</th>
                                                <th>Estimated Duration</th>
                                                <th>Vehicle Type</th>
                                                <th>Date Created</th>
                                                <th>Message</th>
                                                <th>Booking Status</th>
                                                <th>Created Date</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($show as $data)
                                                <tr>
                                                    <td>{{$data->firstname}}</td>
                                                    <td>{{$data->lastname}}</td>
                                                    <td>{{$data->customer_phone}}</td>
                                                    <td>{{$data->customer_email}}</td>
                                                    <td>{{$data->pick_up_date}}</td>
                                                    <td>{{$data->pick_up_time}}</td>
                                                    <td>{{$data->estimated_duration}}</td>
                                                    <td>{{$data->vehicle_type}}</td>
                                                    <td>{{$data->message}}</td>
                                                    <td>{{$data->booking_status}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <a href="{{ route('backend.DeleteBrand',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection