@extends('backend.layout.master')

@section('title', 'DDriver')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Customers</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">

                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Date Created</th>
                                                {{--<th>Edit</th>--}}
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listdrivers as $data)
                                                <tr>
                                                    <td>{{$data->firstname}}</td>
                                                    <td>{{$data->lastname}}</td>
                                                    <td>{{$data->phone}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    {{--<td>--}}

                                                        {{--<button type="button"--}}
                                                                {{--class="btn btn-login btn-min-width mr-1 mb-1"--}}
                                                                {{--data-toggle="modal"--}}
                                                                {{--data-target="#default{{$data->id}}">--}}
                                                            {{--<i class="fas fa-edit"></i>  Edit--}}
                                                        {{--</button>--}}
                                                        {{--<!-- Modal -->--}}
                                                        {{--<div class="modal fade text-left" id="default{{$data->id}}"--}}
                                                             {{--tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"--}}
                                                             {{--aria-hidden="true">--}}
                                                            {{--<div class="modal-dialog" role="document">--}}
                                                                {{--<div class="modal-content">--}}
                                                                    {{--<div class="modal-header">--}}
                                                                        {{--<h4 class="modal-title" id="myModalLabel1">--}}
                                                                            {{--Edit Car Features</h4>--}}
                                                                        {{--<button type="button" class="close"--}}
                                                                                {{--data-dismiss="modal" aria-label="Close">--}}
                                                                            {{--<span aria-hidden="true">&times;</span>--}}
                                                                        {{--</button>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="modal-body">--}}
                                                                        {{--<form class="form-horizontal form-simple" method="POST" action="{{ url('UpdateDriver') }}" enctype="multipart/form-data">--}}
                                                                            {{--{{ csrf_field() }}--}}
                                                                            {{--<div class="form-body">--}}
                                                                                {{--<div class="row">--}}
                                                                                    {{--<div class="col-md-12" hidden>--}}
                                                                                        {{--<div class="form-group">--}}
                                                                                            {{--<label for="projectinput1">First Name</label>--}}
                                                                                            {{--<input type="text" id="projectinput1" class="form-control"--}}
                                                                                                   {{--name="id" value="{{$data->id}}">--}}
                                                                                        {{--</div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-md-12">--}}
                                                                                        {{--<div class="form-group">--}}
                                                                                            {{--<label for="projectinput1">First Name</label>--}}
                                                                                            {{--<input type="text" id="projectinput1" class="form-control"  value="{{$data->driver_firstname}}"--}}
                                                                                                   {{--name="driver_firstname">--}}
                                                                                        {{--</div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-md-12">--}}
                                                                                        {{--<div class="form-group">--}}
                                                                                            {{--<label for="projectinput1">Last Name</label>--}}
                                                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{$data->driver_lastname}}"--}}
                                                                                                   {{--name="driver_lastname">--}}
                                                                                        {{--</div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-md-12">--}}
                                                                                        {{--<div class="form-group">--}}
                                                                                            {{--<label for="projectinput1">Languages</label>--}}
                                                                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{$data->driver_languages}}"--}}
                                                                                                   {{--name="driver_languages">--}}
                                                                                        {{--</div>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-md-12">--}}
                                                                                        {{--<fieldset class="form-group{{ $errors->has('fileToUpload') ? ' has-error' : '' }}">--}}
                                                                                            {{--<label for="projectinput8">Driver Image</label>--}}
                                                                                            {{--<input type="file" class="form-control-file" id="exampleInputFile" name="fileToUpload" value="{{ old('fileToUpload') }}" >--}}
                                                                                            {{--@if ($errors->has('fileToUpload'))--}}
                                                                                                {{--<span class="help-block">--}}
                                                                  {{--<strong>{{ $errors->first('fileToUpload') }}</strong>--}}
                                                                 {{--</span>--}}
                                                                                            {{--@endif--}}
                                                                                        {{--</fieldset>--}}
                                                                                    {{--</div>--}}
                                                                                    {{--<div class="col-lg-12">--}}
                                                                                        {{--<img src="images/{{$data->driver_image}}" style="width:100%">--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                            {{--<div class="form-actions">--}}
                                                                                {{--<button type="submit" class="btn btn-login">--}}
                                                                                    {{--<i class="la la-check-square-o"></i> Save--}}
                                                                                {{--</button>--}}
                                                                            {{--</div>--}}
                                                                        {{--</form>--}}
                                                                    {{--</div>--}}

                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</td>--}}

                                                    <td>
                                                        <a href="{{ route('backend.DeleteBrand',['id'=> $data->id])}}"
                                                           class="btn btn-login btn-min-width mr-1 mb-1"><i
                                                                    class="fas fa-trash"></i> Remove</a></td>
                                                </tr>
                                            @endforeach
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection