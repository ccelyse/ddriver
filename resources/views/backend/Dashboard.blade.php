@extends('backend.layout.master')

@section('title', 'DDriver')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .warning{
            color: #103e5f !important;
        }
    </style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div id="crypto-stats-3" class="row">
                <div class="col-xl-3 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-user-friends warning font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Accounts</h4>
                                    </div>
                                    <div class="col-10 text-right">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-car warning font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Drivers</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <?php echo "$countdriver";?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-chart-line warning font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Bookings</h4>
                                    </div>
                                    <div class="col-10 text-right">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>App Users</h4>
                                    </div>
                                    <div class="col-10 text-right">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection