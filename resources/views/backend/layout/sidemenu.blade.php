<style>
    .main-menu.menu-light .navigation>li.open>a {
        color: #545766;
        background: #f5f5f5;
        border-right: 4px solid #6b442b !important;
    }
</style>
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item"><a href="{{url('Dashboard')}}"><i class="fas fa-tachometer-alt"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a>
            </li>
            <li class=" nav-item"><a href="#"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.templates.main">Account</span></a>
                <ul class="menu-content">
                    <li class=" nav-item"><a href="{{url('CreateAccount')}}"><i class="fas fa-user-alt"></i><span class="menu-title" data-i18n="nav.dash.main">Create Account</span></a>
                    </li>
                    <li class=" nav-item"><a href="{{url('AccountList')}}"><i class="fas fa-user-circle"></i><span class="menu-title" data-i18n="nav.dash.main">Account List</span></a>
                    </li>
                </ul>
            </li>
            {{--<li class=" nav-item"><a href="#"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.templates.main">Cars</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('AddaBrand')}}"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Add a brand</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('BrandModel')}}"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Add a Models</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li class=" nav-item"><a href="{{url('Customers')}}"><i class="fas fa-users"></i><span class="menu-title" data-i18n="nav.dash.main">App Users</span></a>
            <li class=" nav-item"><a href="{{url('AddDrivers')}}"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Drivers</span></a>
            <li class=" nav-item"><a href="{{url('AppBookings')}}"><i class="fas fa-chart-line"></i><span class="menu-title" data-i18n="nav.dash.main">Bookings</span></a>
            {{--<li class=" nav-item"><a href="#"><i class="fas fa-money-bill-alt"></i><span class="menu-title" data-i18n="nav.templates.main">Car Selling</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('CarSell')}}"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Add New</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('CarSellList')}}"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Car Selling List</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class=" nav-item"><a href="#"><i class="fas fa-car-side"></i><span class="menu-title" data-i18n="nav.templates.main">Car Renting</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('CarRent')}}"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Add New</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('CarRentList')}}"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Car Renting List</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class=" nav-item"><a href="{{url('CarSell')}}"><i class="fas fa-money-bill-alt"></i><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Car Selling</span></a>--}}
            {{--<li class=" nav-item"><a href="#"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.templates.main">Cars</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li class=" nav-item"><a href="{{url('AddaBrand')}}"><i class="fas fa-car"></i><span class="menu-title" data-i18n="nav.dash.main">Add a brand</span></a>--}}
                    {{--</li>--}}
                    {{--<li class=" nav-item"><a href="{{url('ListBrands')}}"><i class="fas fa-list-ul"></i><span class="menu-title" data-i18n="nav.dash.main">List of brands</span></a>--}}
                    {{--</li>--}}

                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>


