<footer class="footer navbar-fixed-bottom footer-dark navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 All Rights Reserved.</span>
        <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">An initiative by Kigali Global Shapers & Global Shapers Community

Powered by National Aviation Services (NAS)</span>
    </p>
</footer>