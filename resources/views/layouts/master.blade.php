<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Elysee CONFIANCE">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="images/VFALogo.png">
    <link rel="shortcut icon" type="image/x-icon" href="images/VFALogo.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
          rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
          rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/vendors.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/app.min.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/error.min.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/plugins/ui/jqueryui.css">
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/ui/jquery-ui.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="backend/assets/css/style.css">
    <!-- END Custom CSS-->
    <style>
        .header-navbar .navbar-header {
            width: 260px;
            height: 5rem;
            float: left;
            position: relative;
            padding: 0px 110px !important;
            transition: .3s ease all;
        }
        .header-navbar .navbar-container {
            padding: 0 75px;
        }
    </style>
</head>
<body class="vertical-layout vertical-menu 1-column  bg-cyan bg-lighten-2 menu-expanded fixed-navbar"
      data-open="click" data-menu="vertical-menu" data-col="1-column">

@yield('content')