<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'api_login'], function () {
    Route::get('/MembersList/', 'ListMembers@index');

});

Route::get('/MembersList/{id}', 'ListMembers@show');
Route::post('CustomerSignUp', 'BackendController@CustomerSignUp');
Route::post('CustomerSignIn', 'BackendController@CustomerSignIn');
Route::post('CustomerDetails', 'BackendController@CustomerDetails');
Route::post('BookNow', 'BackendController@BookNow');
Route::post('UserBookings', 'BackendController@UserBookings');
Route::post('CustomerUpdate', 'BackendController@CustomerUpdate');
