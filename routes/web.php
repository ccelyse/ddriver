<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/mail', function () {
//    \Mail::to(User::find(68))->send(new OrderEmail($getOrders,$getguestPhonenumberbold));
//});

Route::get('/',['as'=>'welcome','uses'=>'FrontendController@Home']);
Route::get('/Apply',['as'=>'Apply','uses'=>'FrontendController@Apply']);
Route::post('/Apply_',['as'=>'Apply_','uses'=>'FrontendController@Apply_']);


Route::get('/ContactUs', ['as'=>'ContactUs','uses'=>'FrontendController@ContactUs']);



Route::get('/Test', ['as'=>'backend.Login','uses'=>'FrontendController@Login']);



Auth::routes();

Route::group(['middleware' => ['auth']], function () {
//        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::get('/AddDrivers', ['as' => 'backend.AddDrivers', 'uses' => 'BackendController@AddDrivers']);
        Route::get('/Customers', ['as' => 'backend.Customers', 'uses' => 'BackendController@CustomersAccounts']);
        Route::post('/AddaDriver', ['as' => 'backend.AddaDriver', 'uses' => 'BackendController@AddaDriver']);
        Route::post('/UpdateDriver', ['as' => 'backend.UpdateDriver', 'uses' => 'BackendController@UpdateDriver']);
        Route::get('/AppBookings', ['as' => 'backend.AppBookings', 'uses' => 'BackendController@AppBookings']);

        Route::get('/DeleteBrand', ['as' => 'backend.DeleteBrand', 'uses' => 'BackendController@DeleteBrand']);

        Route::get('/BrandModel', ['as' => 'backend.BrandModel', 'uses' => 'BackendController@BrandModel']);
        Route::post('/BrandModel_', ['as' => 'backend.BrandModel_', 'uses' => 'BackendController@BrandModel_']);
        Route::get('/DeleteBrandModel', ['as' => 'backend.DeleteBrandModel', 'uses' => 'BackendController@DeleteBrandModel']);
        Route::post('/EditBrandName_', ['as' => 'backend.EditBrandName_', 'uses' => 'BackendController@EditBrandName_']);
        Route::post('/getCarModel', ['as' => 'backend.getCarModel', 'uses' => 'BackendController@getCarModel']);


        Route::get('/CarFeatures', ['as' => 'backend.CarFeatures', 'uses' => 'BackendController@CarFeatures']);
        Route::post('/CarFeatures_', ['as' => 'backend.CarFeatures_', 'uses' => 'BackendController@CarFeatures_']);
        Route::post('/EditCarFeatures_', ['as' => 'backend.EditCarFeatures_', 'uses' => 'BackendController@EditCarFeatures_']);
        Route::get('/DeleteCarFeatures_', ['as' => 'backend.DeleteCarFeatures_', 'uses' => 'BackendController@DeleteCarFeatures_']);

//        Route::get('/CarSelling', ['as' => 'backend.CarSelling', 'uses' => 'BackendController@CarSelling']);
        Route::get('/CarSell', ['as' => 'backend.CarSell', 'uses' => 'BackendController@CarSell']);
        Route::get('/CarSellList', ['as' => 'backend.CarSellList', 'uses' => 'BackendController@CarSellList']);
        Route::get('/CarSellEdit', ['as' => 'backend.CarSellEdit', 'uses' => 'BackendController@CarSellEdit']);
        Route::post('/EditCarSellEdit', ['as' => 'backend.EditCarSellEdit', 'uses' => 'BackendController@EditCarSellEdit']);
        Route::get('/CarSellGallery', ['as' => 'backend.CarSellGallery', 'uses' => 'BackendController@CarSellGallery']);
        Route::get('/DeleteCarSellGallery', ['as' => 'backend.DeleteCarSellGallery', 'uses' => 'BackendController@DeleteCarSellGallery']);
        Route::post('/EditCarSellGallery', ['as' => 'backend.EditCarSellGallery', 'uses' => 'BackendController@EditCarSellGallery']);
        Route::get('/DeleteCarSell', ['as' => 'backend.DeleteCarSell', 'uses' => 'BackendController@DeleteCarSell']);

        Route::get('/CarRent', ['as' => 'backend.CarRent', 'uses' => 'BackendController@CarRent']);
        Route::post('/AddCarRent', ['as' => 'backend.AddCarRent', 'uses' => 'BackendController@AddCarRent']);
        Route::get('/CarRentList', ['as' => 'backend.CarRentList', 'uses' => 'BackendController@CarRentList']);
        Route::get('/DeleteCarRent', ['as' => 'backend.DeleteCarRent', 'uses' => 'BackendController@DeleteCarRent']);
        Route::get('/CarRentEdit', ['as' => 'backend.CarRentEdit', 'uses' => 'BackendController@CarRentEdit']);

        Route::post('/ProviceIdAjax', ['as' => 'backend.ProviceIdAjax', 'uses' => 'BackendController@ProviceIdAjax']);
        Route::post('/SelectDistrict', ['as' => 'backend.SelectDistrict', 'uses' => 'BackendController@SelectDistrict']);
        Route::post('/SaveCarRental', ['as' => 'backend.SaveCarRental', 'uses' => 'BackendController@SaveCarRental']);

        Route::get('/AccountList', ['as' => 'backend.AccountList', 'uses' => 'BackendController@AccountList']);
        Route::get('/CreateAccount', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount']);
        Route::post('/CreateAccount_', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount_']);
//        Route::get('PdfView',array('as'=>'backend.PdfView','uses'=>'BackendController@PdfView'));
//        Route::get('/PdfView_', ['as' => 'backend.PdfView_', 'uses' => 'BackendController@PdfView_']);
//        Route::get('/ViewMore', ['as' => 'backend.ViewMore', 'uses' => 'BackendController@ViewMore']);
////        Route::get('/PdfView','BackendController@export_pdf');
//        Route::post('/AddAttractions_', ['as' => 'backend.AddAttractions_', 'uses' => 'BackendController@AddAttractions_']);
});





